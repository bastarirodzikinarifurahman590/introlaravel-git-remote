<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return view('Home');
}) ;
Route::get('/Register', function() {
    return view('Register');
}) ;
Route::get('/Welcom', function() {
    return view('Welcom');
}) ;


Route::get('/', 'HomeController@Home' ) ;
Route::get('/Register', 'AuthController@Register' ) ;
Route::get('/Welcom', 'AuthController@Welcom' ) ;
Route::POST('/Welcom', 'AuthController@Welcom_post' ) ;


